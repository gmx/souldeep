import { useEffect, useState } from "react";
import config from './config'

// todo: to accept type
export function useLyric() {
    const [ lyrics, setLyrics ] = useState([])
    useEffect(() => {
        fetch(`${config.lyric_endpoint}`)   
            .then(res => res.json())
            .then(data => { setLyrics(data.emo) })
    }, [])

    return lyrics
}