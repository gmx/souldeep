import React, { Component, useState } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image, Vibration } from 'react-native';
import { useUnsplash } from './unsplash';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useLyric } from './useLyric';
import { useZip } from './useZip'
// import { Image } from 'react-native-elements';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' + 'Shake or press menu button for dev menu',
});

const colors = {
  "good-night": "#485460",
  "landon-square": "#808e9b",
  "black-pearl": "#1e272e",
  "megaman": "#4bcffa",
  "shadow-steel": "#4b4b4b",
  "baltic-sea": "#3d3d3d"
}

const paths = [
  "https://images.unsplash.com/photo-1515472071456-47b72fb3caff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80",
  "https://images.unsplash.com/photo-1476782916354-326ab24c93df?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1494&q=80",
  "https://images.unsplash.com/photo-1505673542670-a5e3ff5b14a3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80",
  "https://images.unsplash.com/photo-1530335875407-47f48e04b0aa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80",
  "https://scontent-sin2-1.xx.fbcdn.net/v/t1.0-9/p720x720/82889866_2568782216723272_2595405503831998464_o.jpg?_nc_cat=101&_nc_ohc=Vh0XhB4eEiUAX-KixwP&_nc_ht=scontent-sin2-1.xx&_nc_tp=1&oh=3ad4a5407d533dcf5f791f8099f37bce&oe=5EA3530D"
]

function EmoCard({ uri, label }) {
  return (
    <TouchableOpacity activeOpacity={0.7} onTouchStart={() => Vibration.vibrate(60)}>
      <View style={styles.card}>
        <Image source={{ uri: uri }} style={{ flexGrow: 1, borderRadius: 3, backfaceVisibility: "visible" }} blurRadius={0.3} overflow={"visible"} resizeMethod={"scale"} resizeMode={"cover"} />
        <View 
          onTouchMove={(e) => console.log(e)}
          style={{position: "absolute", top: "40%", left: 3, width: "100%", backgroundColor: colors["black-pearl"], padding: 20, opacity: 0.6}}>
          <Text style={{textAlign: "center", color: "white"}}>{label}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default function App() {

  const photos = useUnsplash('sad', 'landscape')
  const lyrics = useLyric()
  const zipped = useZip(photos, lyrics)
  // const label = "ဒါတွေဟာ မင်းလေးကို သိပ်ချစ်လို့..."
  
  return (
    <View style={styles.container}>
      <ScrollView>
        {zipped.map(([ photo, lyric ], index) => (<EmoCard key={index} label={lyric.content} uri={photo.urls.regular} />))}
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors["good-night"],
    paddingTop: 10,
    flex: 1,
    flexDirection: "column",
    alignContent: "flex-start",
  },
  card: {
    backgroundColor: colors["black-pearl"],
    margin: 10,
    borderRadius: 3,
    padding: 3,
    // flexGrow: 0.5,
    height: 300
  }
});
