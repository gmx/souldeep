
export function useZip(arr1, arr2) {
    // todo: filter to skip the empty or null object
    // todo: useMemo 
    if (arr1 != null && arr2 != null) {
        const itratee = arr1.length >= arr2.length ? arr2 : arr1
        return Object.keys(itratee).map(i => {
            return [ arr1[i], arr2[i] ]
        })
    }

    return []
}