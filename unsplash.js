import config from './config'
import {useState, useEffect} from 'react'


export function useUnsplash(term, orientation) {
    const [ photos, setPhotos ] = useState([])
    
    useEffect(() => {
        fetch(`${config.unsplash_endpoint}?client_id=${config.unsplash_client_id}&orientation=${orientation}&query=${term}`)     
            .then(response => response.json())
            .then(data => setPhotos(data.results))
    }, [ term, orientation ]) 

    return photos
}